import { ThemeProvider, useMediaQuery } from '@material-ui/core';
import color from '@material-ui/core/colors/red';
import { createMuiTheme } from '@material-ui/core/styles';
import React, { useLayoutEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Main from './containers/Main';
import { setDarkMode } from './store/app/actions';

export default function App() {

  const darkMode = useSelector((state) => state.app.darkMode);
  const dispatch = useDispatch();
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const theme = useMemo(() =>
    createMuiTheme({
      palette: {
        primary: color,
        type: darkMode ? 'dark' : 'light',
      },
    }),
    [darkMode],
  );

  useLayoutEffect(() => {
    dispatch(setDarkMode(prefersDarkMode));
  }, [dispatch, prefersDarkMode])

  return (
    <ThemeProvider theme={theme}>
      <Main />
    </ThemeProvider>
  );
};
