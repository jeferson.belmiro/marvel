import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import React from 'react';

const useStyles = makeStyles(({
  shadows,
  palette,
  spacing,
  typography,
  transitions,
}) => ({
  card: {
    height: 180,
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    flexGrow: 1,
    border: 0,
    position: 'relative',
    boxShadow: shadows[3],
    overflow: 'hidden',
  },
  cardClickable: {
    transition: 'all 275ms ease',
    cursor: 'pointer',
    transform: 'perspective(300px) translateZ(1px)',
    zIndex: 1,
    transitionTimingFunction: transitions.easing.easeInOut,
    '&:focus': {
      outline: 'none',
    },
    '&:hover': {
      boxShadow: shadows[12],
      zIndex: 9,
      transform: 'perspective(300px) translateZ(20px)',
    },
  },
  title: {
    color: palette.common.white,
    fontWeight: typography.fontWeightMedium,
  },
  details: {
    color: palette.common.white,
    fontSize: 12,
  },
  textContainer: {
    position: 'absolute',
    zIndex: 3,
    bottom: 0,
    left: 0,
    textAlign: 'left',
    margin: spacing(2),
  },
  overlay: {
    position: 'absolute',
    flex: 1,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    zIndex: 1,
    backgroundImage: 'linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.8))',
  },
}))

export default ({ title, details, image, onClick }) => {

  const classes = useStyles();

  return (
    <button
      onClick={(e) => onClick && onClick(e)}
      className={`${classes.card} ${onClick && classes.cardClickable}`}
      style={{ backgroundImage: `url(${image})` }}
    >
      <div className={classes.overlay}>
        <div className={classes.textContainer}>
          <Typography
            className={classes.title}
            type="subheading"
            gutterBottom
          >
            {title}
          </Typography>
          <Typography
            className={classes.details}
            type="body2"
            gutterBottom
          >
            {details}
          </Typography>
        </div>
      </div>
    </button>
  )
};
