import { Alert, AlertTitle } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  container: {
    margin: theme.spacing(2),
  },
}))

export default ({ message }) => {

  const classes = useStyles();

  return (
    <div className={classes.container}>
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        { message }
      </Alert>
    </div>
  );
}

