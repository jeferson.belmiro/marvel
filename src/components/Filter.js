import { Chip, makeStyles, Typography } from '@material-ui/core';
import DescriptionIcon from '@material-ui/icons/Description';
import HighQualityIcon from '@material-ui/icons/HighQuality';
import PhotoIcon from '@material-ui/icons/Photo';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  container: {
    margin: theme.spacing(2),
    display: 'flex',
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  textContainer: {
    display: 'flex',
  },
  text: {
    marginLeft: '5px',
  },
  options: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end',
  }
}));

export default ({ state, onDeleteFilterText, onChangeOption }) => {

  const classes = useStyles();
  const options = Object.keys(state).filter(key => key.startsWith('with'));
  const valueOptions = options.filter(key => state[key]);

  const handleChangeOption = (event, key) => {
    const payload = { [key]: !state[key] }
    onChangeOption && onChangeOption(payload);
  };

  const handleDeleteFilterText = () => {
    onDeleteFilterText && onDeleteFilterText();
  };

  const renderOptions = () => {
    return <div className={classes.toggleContainer}>
      <ToggleButtonGroup
        size="small"
        value={valueOptions}
        aria-label="device">
        <ToggleButton
          value="withDescription"
          onClick={handleChangeOption}
          aria-label="description"
          title="With description">
          <DescriptionIcon />
        </ToggleButton>
        <ToggleButton
          value="withThumbnail"
          onClick={handleChangeOption}
          aria-label="thumbnail"
          title="With thumbnail">
          <PhotoIcon />
        </ToggleButton>
        <ToggleButton
          value="withSeries"
          onClick={handleChangeOption}
          aria-label="series"
          title="With series"
        >
          <HighQualityIcon />
        </ToggleButton>
      </ToggleButtonGroup>
    </div>
  }

  const renderText = () => {
    return <div className={classes.textContainer}>
      <Typography variant="body1">
        Name start with:
      </Typography>
      <Chip
        className={classes.text}
        size="small"
        label={state.text}
        onDelete={handleDeleteFilterText} />
    </div>
  }

  return (
    <div className={classes.container}>
      { state.text && renderText() }
      <div className={classes.options}>
        { renderOptions() }
      </div>
    </div>
  );
}
