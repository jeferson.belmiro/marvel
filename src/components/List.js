import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import Card from './Card';
import Loader from './Loader';

const useStyles = makeStyles((theme) => ({
  content: {
    padding: theme.spacing(),
  },
  item: {
    display: 'flex',
    padding: theme.spacing(),
  },
}));

export default ({ items, hasMore, loadMore, onClick, children, pageStart = 0 }) => {

  const classes = useStyles();

  const renderCard = (item) => {

    const { title, details } = children(item);
    const handleOnPress = onClick ? () => onClick(item) : null;
    const image = `${item.thumbnail.path}/standard_fantastic.${item.thumbnail.extension}`
    return <Card
      onClick={handleOnPress} title={title} details={details} image={image}
    />
  }

  const getScrollParent = () => {
    return document.querySelector('#main-content');
  }

  return (
    <InfiniteScroll
      className={classes.content} pageStart={pageStart} element={'div'}
      loadMore={(page) => loadMore(page)} hasMore={hasMore} threshold={340}
      useWindow={false} loader={<Loader key="loader" />}
      getScrollParent={getScrollParent}
    >
      <Grid container align={'center'}>
        {items.map((item) => (
          <Grid className={classes.item} key={item.id} item xs={12} sm={4} md={2} lg={2}>
            {renderCard(item)}
          </Grid>
        ))}
      </Grid>
    </InfiniteScroll>
  );
}
