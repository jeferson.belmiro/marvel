import { CircularProgress, makeStyles, Typography } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
  loaderContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
  },
  loader: {
    marginRight: theme.spacing(),
  },
}))

export default ({ label = 'Loading...' }) => {
  const classes = useStyles();
  return (
    <div className={classes.loaderContainer}>
      <CircularProgress size={32} className={classes.loader} />
      <Typography type="subheading">
        { label }
      </Typography>
    </div>
  );
};
