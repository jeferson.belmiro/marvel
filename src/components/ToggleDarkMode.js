import { IconButton } from '@material-ui/core';
import Brightness4Icon from '@material-ui/icons/Brightness4';
import Brightness7Icon from '@material-ui/icons/Brightness7';
import React from 'react';

export default ({ onToggle, state }) => {

  const handleClick = () => {
    onToggle && onToggle(!state);
  };

  return (
    <IconButton
      color="inherit"
      aria-label="toggle"
      onClick={handleClick}>
      { state ? <Brightness7Icon /> : <Brightness4Icon /> }
    </IconButton>
  );
}
