import { Divider, makeStyles, Typography } from '@material-ui/core';
import React, { useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import Error from '../components/Error';
import List from '../components/List';
import Loader from '../components/Loader';
import { fetchRequest, fetchSeriesRequest } from '../store/character/actions';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    margin: 'auto',
  },
  section1: {
    // marginBottom: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
    minHeight: '225px',
    overflow: 'hidden',
    justifyContent: 'flex-end',
  },
  image: {
    width: '100%',
    position: "absolute",
    bottom: 0,
    [theme.breakpoints.up('sm')]: {
      top: '-205px',
    },
    [theme.breakpoints.up('md')]: {
      top: '-425px',
    },
  },
  details: {
    color: 'white',
    zIndex: 2,
    margin: theme.spacing(3, 2),
  },
  description: {
    maxWidth: '500px',
  },
  overlay: {
    position: 'absolute',
    flex: 1,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    zIndex: 1,
    backgroundImage: 'linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,0.8))',
  },
  section2: {
    margin: theme.spacing(2),
  },
}));

export default () => {

  const { id } = useParams();
  const state = useSelector((state) => state.character);
  const { data: character, loading, error, series } = state;
  const dispatch = useDispatch();
  const classes = useStyles();

  useLayoutEffect(() => {
    dispatch(fetchRequest(id));
  }, [id, dispatch]);

  const buildSerieCard = (item) => {
    return {
      title: item.title,
      details: `${item.startYear} - ${item.endYear}`,
    }
  };

  const loadMoreSeries = (page) => {
    if (!error) {
      dispatch(fetchSeriesRequest(id, page));
    }
  }

  const render = () => {
    const image = `${character.thumbnail.path}.${character.thumbnail.extension}`
    return <>
      { error && <Error message={String(error)} />}
      <div className={classes.root}>

        <div className={classes.section1}>
          <img className={classes.image} src={image} alt="thumbnail" />
          <div className={classes.overlay}></div>
          <div className={classes.details}>
            <Typography gutterBottom variant="h4">
              {character.name}
            </Typography>
            <Typography className={classes.description} variant="body2">
              {character.description}
            </Typography>
          </div>
        </div>

        <Divider />

        <div className={classes.section2}>
          <Typography gutterBottom variant="body1">
            {character.series.available} Series
          </Typography>
          <div>
            <List items={series.items} hasMore={series.hasMore}
              loadMore={loadMoreSeries}>
              {(item) => buildSerieCard(item)}
            </List>
          </div>
        </div>
      </div>
    </>;
  }

  return loading ? <Loader /> : render();
}
