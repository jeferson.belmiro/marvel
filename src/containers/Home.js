import { Divider } from '@material-ui/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Error from '../components/Error';
import Filter from '../components/Filter';
import List from '../components/List';
import Loader from '../components/Loader';
import { clearFilterText, fetchRequest, fetchFilterOptions } from '../store/characters/actions';

export default () => {

  const history = useHistory();
  const dispatch = useDispatch();
  const state = useSelector((state) => state.characters);
  const { filtered, hasMore, page, loading, error, filter } = state;

  const onClick = (item) => {
    history.push(`/detail/${item.id}`);
  };

  const loadMore = (nextPage) => {
    if (!error) {
      dispatch(fetchRequest(nextPage));
    }
  }

  const makeCard = (item) => {
    return {
      title: item.name,
      details: `${item.series.available} Series`,
    }
  }

  const handleDeleteFilterText = () => {
    dispatch(clearFilterText());
  }

  const handleChangeFilterOption = (payload) => {
    dispatch(fetchFilterOptions(payload));
  }

  const render = () => {
    return <>
      { error && <Error message={String(error)} />}

      <Filter
        state={filter}
        onChangeOption={handleChangeFilterOption}
        onDeleteFilterText={handleDeleteFilterText}
      />

      <Divider variant="middle" />

      <List items={filtered} onClick={onClick}
        hasMore={hasMore} loadMore={loadMore} pageStart={page}>
        {(item) => makeCard(item)}
      </List>
    </>
  }

  return loading ? <Loader /> : render()
}
