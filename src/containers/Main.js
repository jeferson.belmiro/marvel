import { makeStyles } from '@material-ui/core';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import TopBar from './TopBar';
import Detail from './Detail';
import Edit from './Edit';
import Home from './Home';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.text.primary,
    minHeight: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    boxSizing: 'border-box',
    transition: 'background .3s linear',
  },
  topbar: {
    flexGrow: 0,
  },
  content: {
    marginTop: 64,
    overflow: 'auto',
    height: 'calc(100vh - 64px)',
    [theme.breakpoints.down('xs')]: {
      marginTop: 48,
      height: 'calc(100vh - 48px)',
    },
  },
}));

export default function Main() {

  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>

        <div className={classes.topbar}>
          <TopBar />
        </div>

        <div className={classes.content} id="main-content">
          <Switch>
            <Route path="/detail/:id" component={Detail} />
            <Route path="/edit/:id" component={Edit} />
            <Route path="/" component={Home} />
          </Switch>
        </div>

      </div>
    </Router>
  );
};
