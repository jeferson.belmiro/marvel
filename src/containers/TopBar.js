import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import HomeIcon from '@material-ui/icons/Home';
import React from 'react';
import { useHistory } from 'react-router-dom';
import Search from '../components/Search';
import ToggleDarkMode from '../components/ToggleDarkMode';
import { setDarkMode } from '../store/app/actions';
import { fetchFilterText } from '../store/characters/actions';
import { useDispatch, useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  homeButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
}));

export default () => {

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const darkMode = useSelector((state) => state.app.darkMode);

  const handlerClick = () => {
    history.push('/');
  }

  const handlerFilter = (text) => {
    dispatch(fetchFilterText(text));
  }

  const handleToggleDarkMode = (value) => {
    dispatch(setDarkMode(value));
  }

  return (
    <div className={classes.grow}>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.homeButton}
            color="inherit"
            aria-label="go to home"
            onClick={handlerClick}
          >
            <HomeIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            Marvel
          </Typography>
          <div className={classes.grow} />
          <Search onFilter={handlerFilter} />
          <ToggleDarkMode onToggle={handleToggleDarkMode} state={darkMode} />
        </Toolbar>
      </AppBar>
    </div>
  );
};
