import md5 from 'js-md5';

const PUBLIC_KEY = process.env.REACT_APP_PUBLIC_KEY;
const PRIVATE_KEY = process.env.REACT_APP_PRIVATE_KEY;
const BASE_URL = `https://gateway.marvel.com:443`;
const LIMIT_PER_PAGES = 20;

if (!PUBLIC_KEY || !PRIVATE_KEY) {
  throw new Error('invalid keys, configure .env');
}

const makeResponse = async (url) => {

  const response = await fetch(url)
  if (!response.ok) {
    throw new Error(`${response.status} - ${response.statusText}`);
  }
  return await response.json();
};

const makeAuthData = () => {
  const timeStamp = Date.now();
  const hash = md5.create()
  hash.update(timeStamp + PRIVATE_KEY + PUBLIC_KEY)
  return {
    apikey: PUBLIC_KEY,
    ts: timeStamp,
    hash: hash.toString(),
  }
}

const makeQueryParams = (options) => {
  return Object.keys(options).map(function(key) {
    return key + '=' + options[key];
  }).join('&');
}

const makeUrl = (uri, options = {}) => {
  const params = makeQueryParams({ ...makeAuthData(), ...options });
  return `${BASE_URL}${uri}?${params}`;
};

const makePageQueryData = (page) => {

  const offset = page <= 1 ? 0 : (LIMIT_PER_PAGES * (page - 1));
  return { offset, limit: LIMIT_PER_PAGES };
}

export const getCharacters = (page, name = '') => {

  const data = makePageQueryData(page);
  if (name) {
    data.nameStartsWith = name;
  }
  const url = makeUrl('/v1/public/characters', data);
  return makeResponse(url);
};

export const getCharacter = (id) => {
  const uri = `/v1/public/characters/${id}`
  const url = makeUrl(uri);
  return makeResponse(url);
}

export const getSeriesByCharacter = (id, page) => {

  const uri = `/v1/public/characters/${id}/series`
  const data = makePageQueryData(page);
  const url = makeUrl(uri, data);
  return makeResponse(url);
}

