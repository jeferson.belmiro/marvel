
export const SET_DARK_MODE = Symbol('app/set-dark-mode');
export const setDarkMode = (payload) => ({
  type: SET_DARK_MODE,
  payload,
})
