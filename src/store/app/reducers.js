import { SET_DARK_MODE } from "./actions";

const initialState = {
  darkMode: false,
}

export default (state = initialState, action) => {

  switch (action.type) {
    case SET_DARK_MODE: {
      return Object.assign({}, state, {
        darkMode: action.payload
      });
    }

    default:
      return state
  }
}
