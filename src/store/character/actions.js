
export const FETCH_REQUEST = Symbol('character/fetch/request');
export const fetchRequest = (payload) => ({
  type: FETCH_REQUEST,
  payload,
})

export const FETCH_SUCCESS = Symbol('character/fetch/success');
export const fetchSuccess = (payload) => ({
  type: FETCH_SUCCESS,
  payload,
})

export const FETCH_FAILURE = Symbol('character/fetch/failure');
export const fetchFailure = (payload) => ({
  type: FETCH_FAILURE,
  payload,
})

export const FETCH_SERIES_REQUEST = Symbol('character/fetch/series/request');
export const fetchSeriesRequest = (id, page) => ({
  type: FETCH_SERIES_REQUEST,
  id,
  page
})

export const FETCH_SERIES_SUCCESS = Symbol('character/fetch/series/success');
export const fetchSeriesSuccess = (payload) => ({
  type: FETCH_SERIES_SUCCESS,
  payload,
})

export const FETCH_SERIES_FAILURE = Symbol('character/fetch/series/failure');
export const fetchSeriesFailure = (payload) => ({
  type: FETCH_SERIES_FAILURE,
  payload,
})
