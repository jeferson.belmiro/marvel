import {
  FETCH_REQUEST,
  FETCH_FAILURE,
  FETCH_SERIES_SUCCESS,
  FETCH_SERIES_FAILURE,
  FETCH_SUCCESS,
} from "./actions";

const initialState = {
  loading: true,
  error: '',
  data: null,
  series: {
    hasMore: true,
    items: [],
  },
}

export default (state = initialState, action) => {

  switch (action.type) {

    case FETCH_REQUEST: {
      return Object.assign({}, state, {
        loading: true,
        series: {
          hasMore: true,
          items: [],
        },
      })
    }

    case FETCH_SUCCESS: {
      return Object.assign({}, state, {
        loading: false,
        data: action.payload,
      });
    }

    case FETCH_FAILURE: {
      return Object.assign({}, state, {
        loading: false,
        error: action.payload,
      });
    }

    case FETCH_SERIES_SUCCESS: {
      return Object.assign({}, state, {
        series: action.payload
      });
    }

    case FETCH_SERIES_FAILURE: {
      return Object.assign({}, state, {
        series: Object.assign({}, state.series, { hasMore: false }),
        error: action.payload,
      });
    }

    default:
      return state
  }
}
