import { call, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import { getCharacter, getSeriesByCharacter } from '../../services/API';
import {
  fetchFailure,
  fetchSeriesFailure,
  fetchSeriesSuccess,
  fetchSuccess,
  FETCH_REQUEST,
  FETCH_SERIES_REQUEST
} from "./actions";

const getState = (state) => state.character;

function* fetchRequest({ payload: id }) {
  try {

    const response = yield call(getCharacter, id);
    const [ payload ] = response.data.results;
    yield put(fetchSuccess(payload));

  } catch (e) {
    yield put(fetchFailure(e));
  }
}

function* fetchSeriesRequest({ id, page }) {
  try {

    const state = yield select(getState)
    const response = yield call(getSeriesByCharacter, id, page);
    const { results, total, offset, count } = response.data
    const payload = {
      hasMore: total > count && offset < total,
      items: state.series.items.concat(results),
    };
    yield put(fetchSeriesSuccess(payload));

  } catch (e) {
    yield put(fetchSeriesFailure(e));
  }
}

export default () => [
  takeLatest(FETCH_REQUEST, fetchRequest),
  takeEvery(FETCH_SERIES_REQUEST, fetchSeriesRequest),
];
