
export const FETCH_REQUEST = Symbol('characters/fetch/request');
export const fetchRequest = (page) => ({
  type: FETCH_REQUEST,
  page,
})

export const FETCH_SUCCESS = Symbol('characters/fetch/success');
export const fetchSuccess = (payload) => ({
  type: FETCH_SUCCESS,
  payload,
})

export const FETCH_FAILURE = Symbol('characters/fetch/failure');
export const fetchFailure = (payload) => ({
  type: FETCH_FAILURE,
  payload,
})

export const SET_ITEMS_FILTERED = Symbol('characters/set-items-filtered');
export const setItemsFiltered = (payload) => ({
  type: SET_ITEMS_FILTERED,
  payload,
})

export const SET_FILTER = Symbol('characters/set-filter');
export const setFilter = (payload) => ({
  type: SET_FILTER,
  payload,
})

export const SET_FILTER_TEXT = Symbol('characters/set-filter-text');
export const setFilterText = (payload) => ({
  type: SET_FILTER_TEXT,
  payload,
})

export const CLEAR_FILTER_TEXT = Symbol('characters/clear-filter-text');
export const clearFilterText = () => ({
  type: CLEAR_FILTER_TEXT,
})

export const FETCH_FILTER_TEXT = Symbol('characters/fetch-filter-text');
export const fetchFilterText = (payload) => ({
  type: FETCH_FILTER_TEXT,
  payload,
})

export const SET_FILTER_OPTIONS = Symbol('characters/set-filter-options');
export const setFilterOptions = (payload) => ({
  type: SET_FILTER_OPTIONS,
  payload,
})

export const FETCH_FILTER_OPTIONS = Symbol('characters/fetch-filter-options');
export const fetchFilterOptions = (payload) => ({
  type: FETCH_FILTER_OPTIONS,
  payload,
})
