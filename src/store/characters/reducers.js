import {
  FETCH_FAILURE,
  FETCH_SUCCESS,
  SET_FILTER_TEXT,
  SET_ITEMS_FILTERED,
  SET_FILTER_OPTIONS
} from "./actions";

const initialState = {
  loading: false,
  error: '',
  filter: {
    text: '',
    withThumbnail: true,
    withDescription: false,
    withSeries: true,
  },
  hasMore: true,
  items: [],
  filtered: [],
  page: 0,
}

export default (state = initialState, action) => {

  switch (action.type) {

    case FETCH_SUCCESS: {
      return Object.assign({}, state, {
        loading: false,
        ...action.payload,
      });
    }

    case FETCH_FAILURE: {
      return Object.assign({}, state, {
        loading: false,
        hasMore: false,
        error: action.payload,
      });
    }

    case SET_FILTER_TEXT: {
      return Object.assign({}, initialState, {
        loading: true,
        filter: Object.assign({}, state.filter, {
          text: action.payload
        }),
      });
    }

    case SET_FILTER_OPTIONS: {
      return Object.assign({}, state, {
        filter: Object.assign({}, state.filter, action.payload),
      });
    }

    case SET_ITEMS_FILTERED: {
      return Object.assign({}, state, {
        filtered: action.payload
      });
    }

    default:
      return state
  }
}
