import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { getCharacters } from "../../services/API";
import {
  fetchFailure,
  fetchSuccess,
  setFilterText,
  FETCH_REQUEST,
  FETCH_FILTER_TEXT,
  CLEAR_FILTER_TEXT,
  setItemsFiltered,
  setFilterOptions,
  FETCH_FILTER_OPTIONS,
} from "./actions";

const getState = (state) => state.characters;

function applyFilterOptions(items, options) {
  return items.filter((item) => {
    const noSeries = item.series && item.series.available === 0
    const noDescription = String(item.description || '').length === 0;
    const noThumbnail = item.thumbnail && item.thumbnail.path.includes('image_not_available');
    if (options.withSeries && noSeries) {
      return false;
    }
    if (options.withDescription && noDescription) {
      return false;
    }
    if (options.withThumbnail && noThumbnail) {
      return false;
    }
    return true;
  });
}

function* fetchRequest({ page }) {
  try {

    const state = yield select(getState);
    const filter = state.filter.text;
    const response = yield call(getCharacters, page, filter);
    const { results, total, offset, count } = response.data;
    const filtered = applyFilterOptions(results, state.filter);
    const payload = {
      page,
      items: state.items.concat(results),
      filtered: state.filtered.concat(filtered),
      hasMore: total > count && offset < total,
    }

    yield put(fetchSuccess(payload))

  } catch (e) {
    yield put(fetchFailure(e));
  }
}

function* fetchFilterText({ payload: text }) {
  yield put(setFilterText(text));
  yield fetchRequest({ page: 1 });
}

function* clearFilterText() {
  yield fetchFilterText({ payload: '' });
}

function* fetchFilterOptions({ payload }) {

  yield put(setFilterOptions(payload));
  const { items, filter } = yield select(getState);
  const filtered = applyFilterOptions(items, filter);
  yield put(setItemsFiltered(filtered))
}

export default () => [
  takeEvery(FETCH_REQUEST, fetchRequest),
  takeLatest(FETCH_FILTER_TEXT, fetchFilterText),
  takeLatest(CLEAR_FILTER_TEXT, clearFilterText),
  takeLatest(FETCH_FILTER_OPTIONS, fetchFilterOptions),
];
