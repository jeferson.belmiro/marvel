import { combineReducers } from 'redux';
import characters from './characters/reducers';
import character from './character/reducers';
import app from './app/reducers';

export default combineReducers({
  app,
  characters,
  character,
});
