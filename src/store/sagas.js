import { all } from 'redux-saga/effects'
import characters from './characters/sagas'
import character from './character/sagas'

export default function* rootSaga() {
  yield all([
    ...characters(),
    ...character(),
  ])
}
